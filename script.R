library("xlsx")

putQueen <- function(x,y) {
  #print(x)
  #print(y)
  board[,y] <- 1
  board[x,] <- 1
  

  i <- 1
  for (i in 1:n) {
    if(x-i >=1 && y-i >= 1){
      board[x-i,y-i] <- 1
    }
  }
  

  i <- 1
  for (i in 1:n) {
    if(x-i >=1 && y+i <= n){
      board[x-i,y+i] <- 1
    }
  }
  

  i <- 1
  for (i in 1:n) {
    if(x+i <= n && y-i >= 1){
      board[x+i,y-i] <- 1
    }
  }
  

  i <- 1
  for (i in 1:n) {
    if(x+i <= n && y+i <= n){
      board[x+i,y+i] <- 1
    }
  }
  rm(i)
  board[x,y] <- 2
  return(board)
}

##########################
n <- 57
fcount <- 0
emptyrows <- 0
ratings <- matrix(data = 0, nrow = n, ncol = 1)
ratings[1,1] <- NaN

board  <- matrix(data = 0, nrow = n, ncol = n)

#put first Queen:
board <- putQueen(x = 1, y = as.numeric(sample(1:n, 1)))

#loop over board to place Queens
startDtm <- Sys.time()
i <- 2
for (i in 2:n) { #iterate over rows
  
  rowRate <- matrix(data = 0, nrow = 1, ncol = n)
  
  j <- 1
  for (j in 1:n) { #check rates for each column in row
    if(board[i,j] == 0) {
      rowRate[1, j] <- sum(putQueen(i,j) == 0)
      fcount <- fcount+1
    }
    else rowRate[1, j] <- -1
  }
  
  #print(paste("row:", toString(i), "," ,toString(rowRate)))
  
  if(sum(rowRate) != -n){
   #put Queen where row rate was the highest (the first one)
   board <- putQueen(i,which.max(rowRate))
   #store rate
   ratings[i,1] <- max(rowRate)
  } else { #when all places are occupied
    print(paste("row:", toString(i), "empty"))
    emptyrows <- emptyrows +1
  }
}
endDtm <- Sys.time()

results <- rbind(
   c("Chessboard size", n)
  ,c("Heuristic function calls", fcount)
  ,c("Placed queens",sum(board == 2))
  ,c("Free rows",emptyrows)
  ,c("Execution time [s]",(endDtm-startDtm))
)
colnames(results) <- c("Measure name","Value")
colnames(ratings) <- "Ratings per row"

write.xlsx(results, "C:/repozytorium/sys-eksp/results.xlsx", sheetName = "Results", 
           col.names = TRUE, row.names = TRUE, append = FALSE)
write.xlsx(ratings, "C:/repozytorium/sys-eksp/results.xlsx", sheetName = "Ratings", 
           col.names = TRUE, row.names = TRUE, append = TRUE)
write.xlsx(board, "C:/repozytorium/sys-eksp/results.xlsx", sheetName = "Chessboard", 
           col.names = TRUE, row.names = TRUE, append = TRUE)